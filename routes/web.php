<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::model('daylogs', 'Daylog');
Route::model('tasks', 'Task');

Route::bind('daylogs', function($value, $route) {
    return App\Daylog::whereSlug($value)->first();
});

Route::bind('tasks', function($value, $route) {
    return App\Task::whereSlug($value)->first();
});

Route::resource('daylogs', 'DaylogController');
Route::resource('daylogs.tasks', 'TaskController');

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');