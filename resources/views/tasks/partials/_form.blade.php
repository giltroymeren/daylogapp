<div class="form-group">
    {!! Form::label('title', 'Title',
        ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::textarea('title', null,
            [
                'class' => 'form-control',
                'placeholder' => 'Describe your task',
                (isset($isView) ? 'disabled' : null),
            ])
        !!}

        @if ($errors->has('title'))
            @include('layouts.alert-danger', ['errorMessage' => $errors->first('title'),])
        @endif
    </div>
</div>
<div class="form-group">
    {!! Form::label('slug', 'Slug',
        ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('slug', null,
            [
                'class' => 'form-control',
                'placeholder' => 'example-task-slug',
                (isset($isView) ? 'disabled' : null),
            ])
        !!}

        @if ($errors->has('slug'))
            @include('layouts.alert-danger', ['errorMessage' => $errors->first('slug'),])
        @endif
    </div>
</div>
<div class="form-group">
    {!! Form::label('start_at', 'Start Time',
        ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('start_at', null,
            [
                'class' => 'form-control',
                'placeholder' => 'HH:MM:SS',
                (isset($isView) ? 'disabled' : null),
            ])
        !!}

        @if ($errors->has('start_at'))
            @include('layouts.alert-danger', ['errorMessage' => $errors->first('start_at'),])
        @endif
    </div>
</div>
<div class="form-group">
    {!! Form::label('end_at', 'End Time',
        ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('end_at', null,
            [
                'class' => 'form-control',
                'placeholder' => 'HH:MM:SS',
                (isset($isView) ? 'disabled' : null),
            ])
        !!}

        @if ($errors->has('end_at'))
            @include('layouts.alert-danger', ['errorMessage' => $errors->first('end_at'),])
        @endif
    </div>
</div>
<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
        <div class="checkbox">
            <label>
              {!! Form::checkbox('completed') !!} Completed
            </label>
      </div>

        @if ($errors->has('completed'))
            @include('layouts.alert-danger', ['errorMessage' => $errors->first('completed'),])
        @endif
    </div>
</div>

<div class="col-sm-10 col-sm-offset-2
    {{ (isset($isView) ? (($isView) ? 'hidden' : '') : '') }}">
    <div class="form-group">
        {!! Form::submit('Submit', ['class'=>'btn btn-primary']) !!}
        <input type="reset" class="btn btn-danger" />
    </div>
</div>