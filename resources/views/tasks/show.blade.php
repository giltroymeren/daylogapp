@extends('layouts.app')

@section('content')

    @include('layouts.task',
    [
        'method' => 'View',
        'model' => $task,
        'formAttributes' => ['method' => 'GET'],
        'partialsAttributes' => ['isView' => true],
    ])
@endsection