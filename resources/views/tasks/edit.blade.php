@extends('layouts.app')

@section('content')

    @include('layouts.task',
    [
        'method' => 'Edit',
        'model' => $task,
        'formAttributes' =>
            [
                'method' => 'PATCH',
                'route' => ['daylogs.tasks.update', $daylog->slug, $task->slug],
            ],
    ])
@endsection
