@extends('layouts.app')

@section('content')

    @include('layouts.task',
    [
        'method' => 'Create',
        'model' => new App\Task,
        'formAttributes' => ['route' => ['daylogs.tasks.store', $daylog->slug]],
    ])

@endsection
