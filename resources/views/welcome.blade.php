@extends('layouts.app')

@section('content')
    <div class="jumbotron">
        <h1>Day Log Application</h1>
        <p>Manage your daily logs and their tasks.</p>
        <p>
            <a class="btn btn-primary btn-lg" href="{{ url('daylogs') }}" role="button">
                See more
            </a>
        </p>
    </div>
@endsection