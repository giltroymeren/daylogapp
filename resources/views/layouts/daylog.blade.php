<div class="panel panel-default">
    <div class="panel-heading">
        <h3>{{ $method }} Day Log</h3>
    </div>

    <div class="panel-body">
        {!! Form::model( $model, $formAttributes) !!}
            <div class="form-horizontal">
                @include('daylogs/partials/_form',
                    (isset($partialsAttributes) ? $partialsAttributes : []))
            </div>
        {!! Form::close() !!}
    </div>

    @if (!isset($isView))
        <div class="panel-footer">
            <p class="text-center">
                <a href="{{ route('daylogs.index') }}" role="button" class="btn btn-primary">
                    Back
                </a>
            </p>
        </div>
    @endif
</div>