<div class="panel panel-default">
    <div class="panel-heading">
        <h3>
            {{ $method }} Task<br/>
            <small>Day Log "<em>{{ $daylog->title }}</em>"</small>
        </h3>
    </div>

    <div class="panel-body">
        {!! Form::model( $model, $formAttributes) !!}
            <div class="form-horizontal">
                @include('tasks/partials/_form',
                    (isset($partialsAttributes) ? $partialsAttributes : []))
            </div>
        {!! Form::close() !!}
    </div>

    <div class="panel-footer">
        <p class="text-center">
            <a href="{{ route('daylogs.show', $daylog->slug) }}"
                role="button" class="btn btn-primary">
                Back
            </a>
        </p>
    </div>
</div>