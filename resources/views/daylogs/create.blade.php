@extends('layouts.app')

@section('content')

    @include('layouts.daylog',
    [
        'method' => 'Create',
        'model' => new App\Daylog,
        'formAttributes' => ['route' => ['daylogs.store']],
    ])
@endsection
