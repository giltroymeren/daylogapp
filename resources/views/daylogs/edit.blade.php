@extends('layouts.app')

@section('content')

    @include('layouts.daylog',
    [
        'method' => 'Edit',
        'model' => $daylog,
        'formAttributes' =>
            [
                'method' => 'PATCH',
                'route' => ['daylogs.update', $daylog->slug]
            ],
    ])
@endsection
