<div class="form-group">
    {!! Form::label('title', 'Title',
        ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::textarea('title', null,
            [
                'class' => 'form-control',
                'placeholder' => 'Describe your log',
                (isset($isView) ? 'disabled' : null),
            ])
        !!}

        @if ($errors->has('title'))
            @include('layouts.alert-danger', ['errorMessage' => $errors->first('title'),])
        @endif
    </div>
</div>
<div class="form-group">
    {!! Form::label('slug', 'Slug',
        ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('slug', null,
            [
                'class' => 'form-control',
                'placeholder' => 'example-daylog-slug',
                (isset($isView) ? 'disabled' : null),
            ])
        !!}

        @if ($errors->has('slug'))
            @include('layouts.alert-danger', ['errorMessage' => $errors->first('slug'),])
        @endif
    </div>
</div>
<div class="form-group">
    {!! Form::label('location', 'Location',
        ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('location', null,
            [
                'class' => 'form-control',
                'placeholder' => 'Enter your log\'s location',
                (isset($isView) ? 'disabled' : null),
            ])
        !!}

        @if ($errors->has('location'))
            @include('layouts.alert-danger', ['errorMessage' => $errors->first('location'),])
        @endif
    </div>
</div>
<div class="form-group">
    {!! Form::label('log_at', 'Date of Log',
        ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::text('log_at', null,
            [
                'class' => 'form-control',
                'placeholder' => 'YYYY-MM-DD',
                (isset($isView) ? 'disabled' : null),
            ])
        !!}

        @if ($errors->has('log_at'))
            @include('layouts.alert-danger', ['errorMessage' => $errors->first('log_at'),])
        @endif
    </div>
</div>
<div class="form-group">
    {!! Form::label('category', 'Category',
        ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-10">
        {!! Form::select('category',
            [
                'ADEQUATE' => 'Adequate',
                'MINOR' => 'Minor',
                'MAJOR' => 'Major'
            ],
            null,
            [
                'placeholder' => 'Select a category',
                'class' => 'form-control',
                (isset($isView) ? 'disabled' : null),
            ])
        !!}

        @if ($errors->has('category'))
            @include('layouts.alert-danger', ['errorMessage' => $errors->first('category'),])
        @endif
    </div>
</div>

<div class="col-sm-10 col-sm-offset-2
    {{ (isset($isView) ? 'hidden' : '') }}">
    <div class="form-group">
        {!! Form::submit('Submit', ['class'=>'btn btn-primary']) !!}
        <input type="reset" class="btn btn-danger" />
    </div>
</div>