@extends('layouts.app')

@section('content')

    @include('layouts.daylog',
    [
        'method' => 'View',
        'model' => $daylog,
        'formAttributes' => ['method' => 'GET'],
        'partialsAttributes' => ['isView' => true],
        'isView' => true,
    ])

    <div class="panel panel-default">
        <div class="panel-body">
            <h4 class="list-group-item-heading">Tasks</h4>
            @if ( $daylog->tasks->count() )
                <table class="table" id="tasks">
                    <thead>
                        <th>Title</th>
                        <th></th>
                        <th></th>
                    </thead>
                    <tbody>
                        @foreach( $daylog->tasks as $task )
                            <tr data-slug="{{ $task->slug }}">
                                <td>
                                    <a href="{{ route('daylogs.tasks.show',
                                        [$daylog->slug, $task->slug]) }}">
                                        {{ $task->title }}
                                    </a>
                                </td>
                                <td>
                                    {!! link_to_route('daylogs.tasks.edit', 'Edit',
                                        [$daylog->slug, $task->slug],
                                        ['class' => 'btn btn-info btn-xs']
                                    ) !!}
                                </td>
                                <td>
                                    {!! Form::open(
                                        [
                                            'class' => 'form-inline',
                                            'method' => 'DELETE',
                                            'route' => ['daylogs.tasks.destroy',
                                                $daylog->slug, $task->slug],
                                        ]) !!}
                                        {!! Form::submit('Delete',
                                            ['class' => 'btn btn-danger btn-xs']) !!}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                <div class="alert alert-info">Your daylog has no tasks.</div>
            @endif
        </div>

        <div class="panel-footer">
            <p class="text-center">
                <a href="{{ route('daylogs.index') }}" role="button" class="btn btn-primary">
                    Back
                </a>
                <a href="/daylogs/{{ $daylog->slug }}/tasks/create"
                    role="button" class="btn btn-primary">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    Create Task
                </a>
            </p>
        </div>
    </div>

@endsection
