@extends('layouts.app')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading"><h3>Day Logs</h3></div>

        <div class="panel-body">
            @if ( !$daylogs->count() )
            <div class="alert alert-info" role="alert">
                You have no Day Logs.
            </div>
            @else

                <table class="table" id="daylogs">
                    <thead>
                        <tr>
                            <th width="75%">Title</th>
                            <th class="text-right">Date</th>
                            <th class="text-right">Tasks</th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach( $daylogs as $daylog )
                            <tr data-slug="{{ $daylog->slug }}">
                                <td>
                                    <a href="{{ route('daylogs.show', $daylog->slug) }}">
                                        {{ $daylog->title }}
                                    </a>
                                </td>
                                <td class="text-right">
                                    <code>{{ $daylog->log_at }}</code>
                                </td>
                                <td class="text-right">
                                    <code>{{ $daylog->tasks->count() }}</code>
                                </td>
                                <td>
                                    <a href="{{ route('daylogs.edit', $daylog->slug) }}"
                                        class="btn btn-primary btn-xs">
                                        Edit
                                    </a>
                                </td>
                                <td>
                                    {!! Form::open(
                                    [
                                        'class' => 'form-inline',
                                        'method' => 'DELETE',
                                        'route' => ['daylogs.destroy', $daylog->slug]
                                    ]) !!}
                                        <button type="submit" class="btn btn-danger btn-xs">
                                            Delete
                                        </button>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            @endif
        </div>

        <div class="panel-footer">
            <p class="text-center">
                <a href="{{ route('daylogs.create') }}" class="btn btn-primary">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                     Create Day Log
                </a>
            </p>
        </div>
    </div>
@endsection