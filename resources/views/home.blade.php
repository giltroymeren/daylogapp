@extends('layouts.app')

@section('content')
    <div class="col-sm-8">
        <div class="panel panel-default">
            <div class="panel-heading">Dashboard</div>

            <div class="panel-body">
                <p>
                    <a class="btn btn-primary" href="{{ url('daylogs') }}" role="button">
                        <span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> Day Logs
                    </a>
                </p>
            </div>
        </div>
    </div>

    <div class="col-sm-4">
        <div class="panel panel-default">
            <div class="panel-heading">Miscellaneous</div>
            <div class="panel-body">
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Nam sit amet mollis ipsum. Morbi porttitor, nunc sed interdum placerat,
                    leo ligula tempus justo, vel suscipit ligula erat quis turpis.
                </p>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Aenean lacinia erat convallis, venenatis justo id, congue risus.
                    Aenean vel ipsum ipsum.
                </p>
            </div>
        </div>
    </div>
@endsection
