<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SeederTest extends TestCase
{
    public function testCreateDaylogWitTask_shouldBothBeInDatabase()
    {
        $daylog = factory(\App\Daylog::class)->create();
        $task = factory(\App\Task::class)->create([
                'daylog_id' => $daylog->id,
            ]);

        $this->assertDatabaseHas('daylogs', [
            'user_id' => $daylog->user_id,
            'title' => $daylog->title,
            'slug' => $daylog->slug,
            'location' => $daylog->location,
            'log_at' => $daylog->log_at,
            'category' => $daylog->category
        ]);

        $this->assertDatabaseHas('tasks', [
            'daylog_id' => $task->daylog_id,
            'title' => $task->title,
            'slug' => $task->slug,
            'start_at' => $task->start_at,
            'end_at' => $task->end_at,
            'completed' => $task->completed
        ]);

        $daylog->delete();
    }

    public function testCreateDaylogWithNoTask_shouldBeInDatabase()
    {
        $daylog = factory(\App\Daylog::class)->create();

        $this->assertDatabaseHas('daylogs', [
            'user_id' => $daylog->user_id,
            'title' => $daylog->title,
            'slug' => $daylog->slug,
            'location' => $daylog->location,
            'log_at' => $daylog->log_at,
            'category' => $daylog->category
        ]);

        $this->assertDatabaseMissing('tasks', [
            'daylog_id' => $daylog->id,
        ]);

        $daylog->delete();
    }
}
