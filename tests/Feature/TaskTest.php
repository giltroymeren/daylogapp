<?php

namespace Tests\Feature;

use Tests\TestCase;
use Tests\DuskTestCase;
use Laravel\Dusk\Chrome;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TaskTest extends DuskTestCase
{
    private $indexURL = "/daylogs";
    private $loginURL = "/login";

    private $createLinkText = 'Create Task';
    private $submitBtnText = 'Submit';
    private $editBtnText = 'Edit';
    private $titleDaylogPage = 'Day Logs';

    private $user;
    private $daylog;
    private $taskCreate;
    private $taskMake;

    public function setUp()
    {
        parent::setUp();

        $this->user = \App\User::find(1);
        $this->daylog = factory(\App\Daylog::class)->create(['user_id' => $this->user->id]);
        $this->taskCreate = factory(\App\Task::class)->create(['daylog_id' => $this->daylog->id]);
        $this->taskMake = factory(\App\Task::class)->make(['daylog_id' => $this->daylog->id]);
    }

    public function testViewDaylogWithTask_shouldShowPageWithTasks()
    {
        $this->browse(
            function ($browser)
            {
                $browser->visit($this->loginURL)
                    ->loginAs($this->user)

                    ->visit($this->indexURL)
                    ->assertSee($this->titleDaylogPage)
                    ->assertVisible("#daylogs")
                    ->clickLink($this->daylog->title);

                $browser->with('#tasks',
                    function($list)
                    {
                        $list->assertSee($this->taskCreate->title);
                    }
                );
            }
        );
    }

    public function testViewTask_shouldShowPageWithTitle()
    {
        $this->browse(
            function ($browser)
            {
                $browser->visit($this->indexURL)
                    ->assertSee($this->titleDaylogPage)
                    ->assertVisible("#daylogs")
                    ->clickLink($this->daylog->title)
                    ->clickLink($this->taskCreate->title)
                    ->assertSee($this->taskCreate->title);
            }
        );
    }

    public function testCreateWithAllEmpty_shouldShowValidationErrorsForAll()
    {
        $this->browse(
            function ($browser)
            {
                $browser->visit($this->indexURL)
                    ->assertSee($this->titleDaylogPage)
                    ->assertVisible("#daylogs")
                    ->clickLink($this->daylog->title)
                    ->clickLink($this->createLinkText)
                    ->press($this->submitBtnText)
                    ->assertSee("The title field is required.")
                    ->assertSee("The slug field is required.")
                    ->assertSee("The start at field is required.")
                    ->assertSee("The end at field is required.");
            }
        );
    }


    public function testCreateWithAllFilled_shouldShowOKMessageAndExistInHomePageAndDatabase()
    {
        $this->browse(
            function ($browser)
            {
                $browser->visit($this->indexURL)
                    ->assertSee($this->titleDaylogPage)
                    ->assertVisible("#daylogs")
                    ->clickLink($this->daylog->title)
                    ->clickLink($this->createLinkText)
                    ->type('title', $this->taskCreate->title)
                    ->type('slug', $this->taskCreate->slug)
                    ->type('start_at', $this->taskCreate->start_at)
                    ->type('end_at', $this->taskCreate->end_at)
                    ->check('completed')
                    ->press($this->submitBtnText)
                    ->assertPathIs($this->indexURL.'/'.$this->daylog->slug);

                $browser->with('#tasks',
                    function($list)
                    {
                        $list->assertSeeLink($this->taskCreate->title);
                    }
                );

                $this->assertDatabaseHas('tasks', [
                    'title' => $this->taskCreate->title,
                    'slug' => $this->taskCreate->slug,
                ]);
            }
        );
    }

    public function testEdit_shouldSeeOldValuesInForm()
    {
        $this->browse(
            function ($browser)
            {
                $browser->visit($this->indexURL)
                    ->clickLink($this->daylog->title);

                $browser->with('[data-slug='.$this->taskCreate->slug.']',
                    function($row)
                    {
                        $row->clickLink($this->editBtnText)
                            ->assertPathIs(
                                $this->indexURL.'/'.$this->daylog->slug
                                .'/tasks/'.$this->taskCreate->slug.'/edit');
                    }
                );

                $browser->with('form',
                    function($form)
                    {
                        $form->assertInputValue('title', $this->taskCreate->title)
                            ->assertInputValue('slug', $this->taskCreate->slug)
                            ->assertInputValue('start_at', $this->taskCreate->start_at)
                            ->assertInputValue('end_at', $this->taskCreate->end_at);

                        if($this->taskCreate->completed)
                        {
                            $form->assertChecked('completed');
                        }
                        else
                        {
                            $form->assertNotChecked('completed');
                        }
                    }
                );
            }
        );
    }

    public function testEditAndChangeTitle_shouldSeeNewTitleInHomePageAndDatabase()
    {
        $this->browse(
            function ($browser)
            {
                $browser->visit($this->indexURL)
                    ->clickLink($this->daylog->title);

                $browser->with('[data-slug='.$this->taskCreate->slug.']',
                    function($row)
                    {
                        $row->clickLink($this->editBtnText)
                            ->assertPathIs(
                                $this->indexURL.'/'.$this->daylog->slug
                                .'/tasks/'.$this->taskCreate->slug.'/edit');
                    }
                );

                $browser->with('form',
                    function($form)
                    {
                        $form->type('title', $this->taskMake->title)
                            ->press($this->submitBtnText);
                    }
                );

                $browser->assertPathIs($this->indexURL.'/'.$this->daylog->slug
                        .'/tasks/'.$this->taskCreate->slug)
                    ->assertSee($this->taskMake->title);


                $this->assertDatabaseHas('tasks', [
                    'title' => $this->taskMake->title,
                ]);
            }
        );
    }

    public function testDelete_shouldNotBeInDaylogPageAndDatabase()
    {
        $this->browse(
            function ($browser)
            {
                $browser->visit($this->indexURL)
                    ->clickLink($this->daylog->title);

                $browser->with('[data-slug='.$this->taskCreate->slug.']',
                    function($row)
                    {
                        $row->press('Delete')
                            ->assertPathIs(
                                $this->indexURL.'/'.$this->daylog->slug);
                    }
                );

                $browser->assertDontSee($this->taskCreate->title);

                $this->assertDatabaseMissing('tasks', [
                    'id' => $this->taskCreate->id
                ]);
            }
        );
    }

    public function tearDown()
    {
        \App\Daylog::destroy($this->daylog->id);

        parent::tearDown();
    }
}