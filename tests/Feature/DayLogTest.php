<?php

namespace Tests\Feature;

use Tests\TestCase;
use Tests\DuskTestCase;
use Laravel\Dusk\Chrome;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class DaylogTest extends DuskTestCase
{
    private $indexURL = "/daylogs";
    private $loginURL = "/login";

    private $createLinkText = 'Create Day Log';
    private $submitBtnText = 'Submit';

    private $user;
    private $daylogCreate;
    private $daylogMake;

    public function setUp()
    {
        parent::setUp();

        $this->user = \App\User::find(1);
        $this->daylogCreate = factory(\App\Daylog::class)->create(['user_id' => $this->user->id]);
        $this->daylogMake = factory(\App\Daylog::class)->make(['user_id' => $this->user->id]);
    }

    public function testVisitIndexPage_shouldShowListOfDaylogs()
    {
        $this->browse(
            function ($browser)
            {
                $browser->visit($this->loginURL)
                    ->loginAs($this->user)

                    ->visit($this->indexURL)
                    ->assertSee("Day Logs")
                    ->assertVisible("#daylogs")
                    ->assertSeeLink($this->daylogCreate->title);
            }
        );
    }

    public function testVisitIndexPage_clickingOneLink_shouldShowShowPage()
    {
        $this->browse(
            function ($browser)
            {
                $browser->visit($this->indexURL)
                    ->assertSeeLink($this->daylogCreate->title)
                    ->clickLink($this->daylogCreate->title)
                    ->assertPathIs($this->indexURL.'/'.$this->daylogCreate->slug)
                    ->assertSee($this->daylogCreate->title);
            }
        );
    }

    public function testCreateWithAllEmpty_shouldShowValidationErrorsForAll()
    {
        $this->browse(
            function ($browser)
            {
                $browser->visit($this->indexURL)
                    ->assertSeeLink($this->createLinkText)
                    ->clickLink($this->createLinkText)
                    ->assertSee($this->createLinkText)
                    ->press($this->submitBtnText)
                    ->assertSee('The title field is required.')
                    ->assertSee('The slug field is required.')
                    ->assertSee('The location field is required.')
                    ->assertSee('The log at field is required.')
                    ->assertSee('The category field is required.');
            }
        );
    }

    public function testCreateWithAllFilled_shouldShowOKMessageAndExistInHomePageAndDatabase()
    {
        $this->browse(
            function ($browser)
            {
                $browser->visit($this->indexURL)
                    ->assertSeeLink($this->createLinkText)
                    ->clickLink($this->createLinkText)
                    ->assertSee($this->createLinkText)

                    ->type('title', $this->daylogMake->title)
                    ->type('slug', $this->daylogMake->slug)
                    ->type('location', $this->daylogMake->location)
                    ->value('#log_at', $this->daylogMake->log_at)
                    ->select('category', $this->daylogMake->category)
                    ->press($this->submitBtnText)

                    ->assertPathIs($this->indexURL)
                    ->assertSeeLink($this->daylogMake->title);

                $this->assertDatabaseHas('daylogs', [
                    'title' => $this->daylogMake->title,
                    'slug' => $this->daylogMake->slug,
                    'log_at' => $this->daylogMake->log_at,
                ]);
            }
        );
    }

    public function testEditAndChangeTitleAndLogAt_shouldSeeNewTitleAndLogAtInHomePageAndDatabase()
    {
        $this->browse(
            function ($browser)
            {
                $browser->visit($this->indexURL);

                $browser->with('[data-slug='.$this->daylogCreate->slug.']',
                    function($row)
                    {
                        $row->clickLink('Edit')
                            ->assertPathIs($this->indexURL.'/'.$this->daylogCreate->slug.'/edit');
                    });

                $browser->with('form',
                    function($form)
                    {
                        $form->assertInputValue('title', $this->daylogCreate->title)
                            ->assertInputValue('slug', $this->daylogCreate->slug)
                            ->assertInputValue('location', $this->daylogCreate->location)
                            ->assertInputValue('log_at', $this->daylogCreate->log_at)
                            ->assertSelected('category', $this->daylogCreate->category)

                            ->type('title', $this->daylogMake->title)
                            ->type('log_at', $this->daylogMake->log_at)
                            ->press($this->submitBtnText);
                    });

                $browser->assertInputValue('title', $this->daylogMake->title);

                $this->assertDatabaseHas('daylogs', [
                    'title' => $this->daylogMake->title,
                    'log_at' => $this->daylogMake->log_at,
                ]);
            }
        );
    }

    public function testDelete_shouldNotBeInIndexPageAndDatabase()
    {
        $this->browse(
            function ($browser)
            {
                $browser->visit($this->indexURL);

                $browser->with('[data-slug='.$this->daylogCreate->slug.']',
                    function($row)
                    {
                        $row->press('Delete');
                    });

                $browser->assertDontSee($this->daylogCreate->title);

                $this->assertDatabaseMissing('daylogs', [
                    'id' => $this->daylogCreate->id
                ]);
            }
        );
    }

    public function tearDown()
    {
        \App\Daylog::destroy($this->daylogCreate->id);

        parent::tearDown();
    }
}
