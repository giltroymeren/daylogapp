<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        for($i = 0; $i < 5; $i++)
        {
            factory(\App\User::class)->create();
        }
    }
}
