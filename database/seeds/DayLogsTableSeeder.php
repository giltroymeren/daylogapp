<?php

use Illuminate\Database\Seeder;

class DaylogsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('daylogs')->delete();

        for($i = 0; $i < 25; $i++)
        {
            factory(\App\Daylog::class)->create();
        }
    }
}
