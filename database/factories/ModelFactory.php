<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Daylog::class, function (Faker\Generator $faker) {
    return [
        // NOTE: Range depends on UsersTableSeeder::run()
        'user_id' => $faker->numberBetween($min = 1, $max = 5),
        'title' => $faker->paragraph,
        'slug' => $faker->slug,
        'location' => $faker->streetAddress,
        // NOTE: Mirrors the format of MySQL `date` format of YYYY-MM-DD
        'log_at' => $faker->unique()->dateTimeThisDecade($max = 'now',
            $timezone = date_default_timezone_get())->format('Y-m-d'),
        'category' => $faker->randomElement($array = array('ADEQUATE', 'MINOR', 'MAJOR'))
    ];
});

$factory->define(App\Task::class, function (Faker\Generator $faker) {
    return [
        // NOTE: Range depends on DayLogsTableSeeder::run()
        'daylog_id' => $faker->numberBetween($min = 1, $max = 25),
        'title' => $faker->text($maxNbChars = 255),
        'slug' => $faker->slug,
        'start_at' => "10:10:10",
        'end_at' => "11:11:11",
        'completed' => $faker->randomElement($array = array(true, false))
    ];
});

