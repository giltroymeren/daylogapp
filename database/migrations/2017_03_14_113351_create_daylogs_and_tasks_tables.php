<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDaylogsAndTasksTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daylogs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 1024)->default('');
            $table->integer('user_id')->unsigned()->default(0);
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('slug')->default('');
            $table->string('location')->default('');
            $table->date('log_at')->unique()->default(date_format(new DateTime(), 'Y-m-d'));
            $table->enum('category', array('ADEQUATE', 'MINOR', 'MAJOR'));
            $table->timestamps();
        });

        Schema::create('tasks', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('daylog_id')->unsigned()->default(0);
            $table->foreign('daylog_id')->references('id')->on('daylogs')->onDelete('cascade');
            $table->string('title')->default('');
            $table->string('slug')->default('');
            $table->time('start_at')->default(date_format(new DateTime(), 'H:i'));
            $table->time('end_at')->default(date_format(new DateTime(), 'H:i'));
            $table->boolean('completed')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
        Schema::dropIfExists('daylogs');
    }
}
