<?php

namespace App\Http\Controllers;

use App\Daylog;
use App\Task;
use ControllerHelper;
use Request;

class TaskController extends Controller
{
    protected $rules = [
        'title' => 'required|max:255',
        'slug' => 'required',
        'start_at' => 'required|date_format:H:i:s|before:end_at',
        'end_at' => 'required|date_format:H:i:s|after:start_at',
    ];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \App\Task $task
     * @return Response
     */
    public function index(Task $task)
    {
        return view('tasks.index', compact('task'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Daylog $daylog
     * @param  \App\Task    $task
     * @return Response
     */
    public function show(Daylog $daylog, Task $task)
    {
        return view('tasks.show', compact('daylog', 'task'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \App\Daylog $daylog
     * @return Response
     */
    public function create(Daylog $daylog)
    {
        return view('tasks.create', compact('daylog'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Daylog $daylog
     * @return Response
     */
    public function store(Daylog $daylog, \Illuminate\Http\Request $request)
    {
        $this->validate($request, $this->rules);

        $input = Request::all();
        $input['daylog_id'] = $daylog->id;
        Task::create( $input );

        return redirect('daylogs/'.$daylog->slug)->with('message',
            ControllerHelper::getNotificationMessage(Task::class, $request->title, 'created'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Daylog $daylog
     * @param  \App\Task    $task
     * @return Response
     */
    public function edit(Daylog $daylog, Task $task)
    {
        return view('tasks.edit', compact('daylog', 'task'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Daylog $daylog
     * @param  \App\Task    $task
     * @return Response
     */
    public function update(Daylog $daylog, Task $task, \Illuminate\Http\Request $request)
    {
        $this->validate($request, $this->rules);

        $input = array_except(Request::all(), '_method');
        $task->update($input);

        return redirect('daylogs/'.$daylog->slug.'/tasks/'.$task->slug)
            ->with('message',
                ControllerHelper::getNotificationMessage(Task::class, $task->title, 'updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Daylog $daylog
     * @param  \App\Task    $task
     * @return Response
     */
    public function destroy(Daylog $daylog, Task $task)
    {
        $title = $task->title;
        $task->delete();

        return redirect('daylogs/'.$daylog->slug)->with('message',
            ControllerHelper::getNotificationMessage(Task::class, $title, 'deleted'));
    }
}
