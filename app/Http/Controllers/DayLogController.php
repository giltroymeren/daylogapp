<?php

namespace App\Http\Controllers;

use App\Daylog;
use ControllerHelper;
use Request;

class DaylogController extends Controller
{
    private function getRules($daylog = null)
    {
        $rules = [
            'title' => 'required|max:1024',
            'slug' => 'required|max:255',
            'location' => 'required|max:255',
            'log_at' => 'required|date_format:Y-m-d|unique:daylogs,log_at'
                . (isset($daylog) ? ','.$daylog->id : ''),
            'category' => 'required|in:ADEQUATE,MINOR,MAJOR',
        ];

        return $rules;
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $daylogs = Daylog::all()->where('user_id', \Auth::user()->id);
        return view('daylogs.index', compact('daylogs'));
    }

    public function show(Daylog $daylog)
    {
        return view('daylogs.show', compact('daylog'));
    }

    public function create()
    {
        return view('daylogs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return Response
     */
    public function store(\Illuminate\Http\Request $request)
    {
        $this->validate($request, $this->getRules());

        $input = Request::all();
        $input['user_id'] = \Auth::user()->id;
        Daylog::create( $input );

        return redirect('daylogs')->with('message',
            ControllerHelper::getNotificationMessage(Daylog::class, $request->title, 'created'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Daylog $daylog
     * @return Response
     */
    public function edit(Daylog $daylog)
    {
        return view('daylogs.edit', compact('daylog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Daylog $daylog
     * @param \Illuminate\Http\Request $request
     * @return Response
     */
    public function update(Daylog $daylog, \Illuminate\Http\Request $request)
    {
        $this->validate($request, $this->getRules($daylog));

        $input = array_except(Request::all(), '_method');
        $daylog->update($input);

        return redirect('daylogs/'.$daylog->slug)->with('message',
            ControllerHelper::getNotificationMessage(Daylog::class, $daylog->title, 'updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Daylog $daylog
     * @return Response
     */
    public function destroy(Daylog $daylog)
    {
        $title = $daylog->title;
        $daylog->delete();

        return redirect('daylogs')->with('message',
            ControllerHelper::getNotificationMessage(Daylog::class, $title, 'deleted'));
    }
}
