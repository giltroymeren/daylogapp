<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Daylog extends Model
{
    protected $table = 'daylogs';

    protected $guarded = [];

    /**
     * Get the route key for the model
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function tasks()
    {
        return $this->hasMany('App\Task');
    }
}
