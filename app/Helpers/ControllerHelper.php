<?php

namespace App\Helpers;

class ControllerHelper
{
    public static function getNotificationMessage($model, $value, $method)
    {
        return sprintf('%s "%s" %s', class_baseName($model), $value, $method);
    }
}