[comment]: # (Tested with http://spec.commonmark.org/dingus/)

# DAY LOG APP


## Table of Contents

  * [About](#about)
  * [Dependencies](#dependencies)
  * [Installation](#installation)
  * [Troubleshooting](#troubleshooting)
  * [Development](#development)
  * [Acknowledgements](#acknowledgements)
  * [Changelog](#changelog)


<a name="about"></a>

## About

`DAY LOG APP` is simple application that enables users
to create, view, update or delete daily logs and their tasks.
It is a personal project created to explore web development with [Laravel](https://laravel.com/).


<a name="dependencies"></a>

## Dependencies

Please see the [Laravel 5.4 official installation guide](https://laravel.com/docs/5.4/installation).


<a name="installation"></a>

## Installation

Please see the [Laravel 5.4 official installation guide](https://laravel.com/docs/5.4/installation).


<a name="troubleshooting"></a>

## Troubleshooting

Unfortunately I have no intention of maintaining this application
in case of bugs as of the moment because I only made this
to study the basics of Laravel and to share what I have learned during the development
(because I have to keep track of the exact steps and a reference in case the need arises).

However I would be my pleasure to receive improvements on the different parts of the codebase.
If I have available time I might update the repository with the suggestions to share them with everyone.

Please feel free to send them at `giltroymeren+daylogapp@gmail.com`.


<a name="development"></a>

## Development

The steps on how this application was created from a fresh Laravel 5.4 installation
is available (!) in [link to follow].


<a name="acknowledgements"></a>

## Acknowledgements

This project is a combination of lessons learned from the following guides:

* Laravel 5.2 [Basic Task List](https://laravel.com/docs/5.2/quickstart)
* Laravel 5.2 [Intermediate Task List](https://laravel.com/docs/5.2/quickstart-intermediate)
* "[Creating a Basic ToDo Application in Laravel 5](https://www.flynsarmy.com/2015/02/creating-a-basic-todo-application-in-laravel-5-part-1/)" series by Flynsamy
* ["Step by Step Guide to building your first Laravel Application"](https://laravel-news.com/your-first-laravel-application) by Eric L. Barnes


<a name="changelog"></a>

[comment]: # (http://keepachangelog.com/en/0.3.0/)

## Changelog

    ## [Unreleased]
    ### Added
    - Chapters:
      - "Dependencies"
      - "Installation"
      - "Troubleshooting"
      - "Development"
      - "Acknowledgements"
      - "Changelog"
    ### Removed
    - "Core components" section from About chapter
    - "Development" chapter

    [Unreleased]: TBA
